<p align="center">
  <img src="https://raw.githubusercontent.com/PKief/vscode-material-icon-theme/ec559a9f6bfd399b82bb44393651661b08aaf7ba/icons/folder-markdown-open.svg" width="20%" alt="TEST-RUNNERS-logo">
</p>
<p align="center">
    <h1 align="center">TEST-RUNNERS</h1>
</p>
<p align="center">
    <em>Testez vos Runners, Optimisez votre CI/CD !</em>
</p>
<p align="center">
	<img src="https://img.shields.io/forge/license/tsanson/test-runners?style=default&logo=opensourceinitiative&logoColor=white&color=0080ff" alt="license">
	<img src="https://img.shields.io/forge/last-commit/tsanson/test-runners?style=default&logo=git&logoColor=white&color=0080ff" alt="last-commit">
	<img src="https://img.shields.io/forge/languages/top/tsanson/test-runners?style=default&color=0080ff" alt="repo-top-language">
	<img src="https://img.shields.io/forge/languages/count/tsanson/test-runners?style=default&color=0080ff" alt="repo-language-count">
</p>

<br>

##### 🔗 Table des Matières

- [📍 Aperçu](#-aperçu)
- [👾 Fonctionnalités](#-fonctionnalités)
- [📂 Structure du Projet](#-structure-du-projet)
- [🧩 Composants](#-composants)
- [🚀 Démarrage](#-démarrage)
    - [🔖 Prérequis](#-prérequis)
    - [📦 Installation](#-installation)
    - [🤖 Utilisation](#-utilisation)
    - [🧪 Tests](#-tests)
- [📌 Feuille de Route](#-feuille-de-route)
- [🤝 Contribution](#-contribution)
- [🎗 Licence](#-licence)
- [🙌 Remerciements](#-remerciements)

---

## 📍 Aperçu

TEST-RUNNERS est un projet conçu pour tester et valider les fonctionnalités des runners GitLab. Il utilise un fichier `.gitlab-ci.yml` pour définir une série de jobs qui testent différents aspects de l'environnement CI/CD de GitLab, permettant ainsi d'assurer la fiabilité et les performances des pipelines.

---

## 👾 Fonctionnalités

|    | Fonctionnalité | Description |
|----|----------------|-------------|
| 🐚 | **Tests Shell** | Exécution de commandes shell de base pour vérifier l'environnement |
| 🌍 | **Variables d'Environnement** | Vérification des variables d'environnement GitLab CI |
| 📦 | **Artefacts** | Création et utilisation d'artefacts entre les jobs |
| 💾 | **Cache** | Test du mécanisme de cache de GitLab |
| 🐳 | **Docker** | Exécution de jobs dans des conteneurs Docker |
| ⚡ | **Parallélisme** | Tests en parallèle pour vérifier les capacités multi-exécution |
| 🧮 | **Tests Matriciels** | Exécution de tests sur différentes combinaisons de plateformes et versions |
| 🧠 | **Test de Mémoire** | Vérification de la gestion de la mémoire des runners |

---

## 📂 Structure du Projet

```sh
└── test-runners/
    ├── .gitlab-ci.yml
    └── README.md
```

---

## 🧩 Composants

Le projet se compose principalement du fichier `.gitlab-ci.yml` qui définit les différentes étapes et jobs du pipeline CI/CD pour tester les runners GitLab.

---

## 🚀 Démarrage

### 🔖 Prérequis

- Une instance GitLab avec des runners configurés
- Accès à un compte GitLab avec les permissions nécessaires pour exécuter des pipelines CI/CD

### 📦 Installation

1. Clonez le dépôt TEST-RUNNERS :
```sh
git clone https://forge.apps.education.fr/tsanson/test-runners
```

2. Naviguez vers le répertoire du projet :
```sh
cd test-runners
```

### 🤖 Utilisation

Pour utiliser ce projet :

1. Poussez le code vers votre dépôt GitLab
2. Observez l'exécution du pipeline dans l'interface CI/CD de GitLab
3. Analysez les résultats de chaque job pour valider les fonctionnalités des runners

### 🧪 Tests

Les tests sont automatiquement exécutés lors du déclenchement du pipeline CI/CD. Vous pouvez consulter les résultats dans l'interface GitLab CI/CD.

---

## 📌 Feuille de Route

- [X] Implémenter les tests de base pour les runners GitLab
- [ ] Ajouter des tests pour les fonctionnalités avancées de GitLab CI/CD
- [ ] Développer un tableau de bord pour visualiser les résultats des tests

---

## 🤝 Contribution

Les contributions sont les bienvenues ! Voici comment vous pouvez contribuer :

- Forkez le projet
- Créez une branche pour votre fonctionnalité (`git checkout -b nouvelle-fonctionnalite`)
- Committez vos changements (`git commit -am 'Ajout d'une nouvelle fonctionnalité'`)
- Poussez vers la branche (`git push origin nouvelle-fonctionnalite`)
- Créez une nouvelle Pull Request

---

## 🎗 Licence

Ce projet est sous licence [INSÉRER LE TYPE DE LICENCE]. Pour plus de détails, consultez le fichier [LICENSE](LICENSE).

---

## 🙌 Remerciements

- L'équipe GitLab pour leur excellente plateforme CI/CD
- Tous les contributeurs qui ont participé à l'amélioration de ce projet

---